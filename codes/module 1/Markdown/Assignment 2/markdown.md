# EXPERIMENT 
### *Aim*: To Study Minor Losses for the flow of fluid through pipe.
### *Apparatus*:A flow circuit of G. I. pipes of different pipe fittings viz. Large bend, Small bend, Elbow, Sudden enlargement from 25 mm dia to 50 mm dia, Sudden contraction from 50 mm dia to 25 mm dia, U-tube differential manometer, collecting tank.
### *Procedure*:
1. Click on the drop-down menu to select the type of loss.
2. Click on the **CONNECT MANOMETER** button to connect the manometer to desired pressure plates.
3. Click on **OPEN CONTROL VALVE** button to open the control valve in order to control the water pressure.The control valve will turn green.
4. Click on the **START** button to start the pump and the water flow. The water will start to flow.
5. As the water flows, observe the change in the U-tube manometer reading according to the connections done with pressure plates.
6. To start the timer, close discharge valve by clicking **CLOSE DISCHARGE VALVE** button. The discharge valve will turn red and the water will start rising in the tank.
7. Observe the rise in level of water in the tank and click the **STOP** button when the water reaches 10cm mark.The timer will stop. Observe the readings in the observation table.
8. Now click on **RESET** button to reset the setup.
9. Now repeat the procedure for sudden contraction.
10. Calculate the values for V1, V2, hL, KL with the help of formulae provided.
11. Enter the calculated rounded-off values upto second decimal place in the respective text boxes.
12. Click on **CHECK ANSWERS** to check the answers.

### *Diagram*:
![Minor losses](minor%20losses.png)


 
> You can virtually perform this experiment from vlab dev site. <br>
> link:
> [Vlabs](http://vlabs.iitb.ac.in/vlabs-dev/labs/mit_bootcamp/fluid_mechanics/experiments/minor-losses-pvg/references.html "Click here")






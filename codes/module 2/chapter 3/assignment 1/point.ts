function check() // used to check if the point p lies inside the triangle or not
{
    let t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    let t3:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    let t4:HTMLInputElement=<HTMLInputElement>document.getElementById("t4");
    let t5:HTMLInputElement=<HTMLInputElement>document.getElementById("t5");
    let t6:HTMLInputElement=<HTMLInputElement>document.getElementById("t6");
    let t7:HTMLInputElement=<HTMLInputElement>document.getElementById("t7");
    let t8:HTMLInputElement=<HTMLInputElement>document.getElementById("t8");
    let ans:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("ans");

    var x1:number=parseFloat(t1.value);
    var y1:number=parseFloat(t2.value);
    var x2:number=parseFloat(t3.value);
    var y2:number=parseFloat(t4.value);
    var x3:number=parseFloat(t5.value);
    var y3:number=parseFloat(t6.value);
    var x:number=parseFloat(t7.value);
    var y:number=parseFloat(t8.value);

    var area1:number=Math.abs((x1*(y2-y3) + x2*(y3-y1) + x3*(y1-y2))/2);  
    console.log(area1);
    var area2:number=Math.abs((x*(y1-y2) + x1*(y2-y) + x2*(y-y1))/2);
    console.log(area2);
    var area3:number=Math.abs((x*(y2-y3) + x2*(y3-y) + x3*(y-y2))/2);
    console.log(area3);
    var area4:number=Math.abs((x*(y1-y3) + x1*(y3-y) + x3*(y-y1))/2);
    console.log(area4);
    
    var sum:number=area2+area3+area4;
    console.log(sum);

    if( Math.abs(area1-sum) < 0.000001)    //condition for checking if point lies inside or not
    
    {
        document.getElementById("ans").innerHTML=" The point lies <b> inside </b> the triangle ";
    }
    else 
    {
        document.getElementById("ans").innerHTML="The point lies <b> outside </b> the triangle";
    }
    
}